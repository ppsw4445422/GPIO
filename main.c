#include <LPC21xx.H>
//edycja pliku v.7
#define LED0_bm (1<<16) 
#define LED1_bm (1<<17) 
#define LED2_bm (1<<18) 
#define LED3_bm (1<<19) 

#define BUTTON0_bm (1<<4)
#define BUTTON1_bm (1<<5)
#define BUTTON2_bm (1<<6)
#define BUTTON3_bm (1<<7)

 
enum KeyboardState {RELASED,BUTTON_0, BUTTON_1, BUTTON_2, BUTTON_3};

enum Step{LEFT, RIGHT};

void ButtonInit(void){
	IO0DIR=IO0DIR&(~(BUTTON0_bm|BUTTON1_bm|BUTTON2_bm|BUTTON3_bm));
}

void Delay(int iTimeInMs){
	int iCycle;
	int iNumberOfCycles = 12000 * iTimeInMs;
	
	for (iCycle = 0; iCycle < iNumberOfCycles; iCycle++) {}
}


void LedInit(void){
	IO1DIR=IO1DIR|(LED0_bm|LED1_bm|LED2_bm|LED3_bm);
	IO1SET= LED0_bm;
}
	
void LedOn(unsigned char ucLedIndeks){
	
	IO1CLR = LED0_bm|LED1_bm|LED2_bm|LED3_bm;
	switch (ucLedIndeks) {
		case 0:
			IO1SET = LED0_bm;
			break;
		case 1:
			IO1SET = LED1_bm;
			break;
		case 2:
			IO1SET = LED2_bm;
			break;
		case 3:
			IO1SET = LED3_bm;
			break;
		default:
			break;
	}
}

enum KeyboardState eKeyboardRead(void){
	
	if((IO0PIN&BUTTON0_bm) == 0){
		return(BUTTON_0);
	}
	if((IO0PIN&BUTTON1_bm) == 0){
		return(BUTTON_1);
	}
	if((IO0PIN&BUTTON2_bm) == 0){
		return(BUTTON_2);
	}
	if((IO0PIN&BUTTON3_bm) == 0){
		return(BUTTON_3);
	}
	return(RELASED);
}

void LedStep(enum Step Direction){

	static unsigned int uiDiode;

	if (Direction == RIGHT ){
		uiDiode++;
	}else{
		uiDiode--;
	}
	LedOn(uiDiode%4);
}

void LedStepRight(void){
		LedStep(RIGHT);
}

void LedStepLeft(void){
		LedStep(LEFT);
}

int main(){	

	LedInit();
	ButtonInit();
	
	while(1){
		Delay(100);
		switch(eKeyboardRead()){
			case BUTTON_1:
				LedStepRight();
				break;			
			case BUTTON_2:
				LedStepLeft();
				break;
			case BUTTON_0:
				break;
			case BUTTON_3:
				break;
			case RELASED:
				break;
			default:{}
		}
	}
}
